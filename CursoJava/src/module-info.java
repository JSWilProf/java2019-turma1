
module CursoJava {
	requires java.desktop;
	requires java.sql;
	
	requires static lombok;
	requires static org.mapstruct.processor;
}