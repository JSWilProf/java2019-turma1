package br.senai.sp.informatica.jdbc.exemplos.cadastro;

import lombok.Data;

@Data
public class Fornecedor {
	private Integer idfornecedor;
	private String nome;
	private String endereco;
}
