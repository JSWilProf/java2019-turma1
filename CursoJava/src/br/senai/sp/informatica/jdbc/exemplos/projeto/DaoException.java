package br.senai.sp.informatica.jdbc.exemplos.projeto;

@SuppressWarnings("serial")
public class DaoException extends Exception {
	public DaoException(String message) {
		super(message);
	}
}
