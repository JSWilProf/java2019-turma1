package br.senai.sp.informatica.jdbc.exemplos.projetoX;

@SuppressWarnings("serial")
public class DaoException extends Exception {
	public DaoException(String message) {
		super(message);
	}
}
