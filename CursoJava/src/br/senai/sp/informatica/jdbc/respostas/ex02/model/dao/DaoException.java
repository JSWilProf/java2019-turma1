package br.senai.sp.informatica.jdbc.respostas.ex02.model.dao;

@SuppressWarnings("serial")
public class DaoException extends Exception {
	public DaoException(String msg) {
		super(msg);
	}
}
