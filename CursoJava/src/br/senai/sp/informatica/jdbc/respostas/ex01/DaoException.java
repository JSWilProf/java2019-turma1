package br.senai.sp.informatica.jdbc.respostas.ex01;

@SuppressWarnings("serial")
public class DaoException extends Exception {
	public DaoException(String message) {
		super(message);
	}
}
