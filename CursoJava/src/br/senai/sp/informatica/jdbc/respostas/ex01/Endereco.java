package br.senai.sp.informatica.jdbc.respostas.ex01;

import lombok.Data;

@Data
public class Endereco {
	private Integer idEndereco;
	private String logradouro;
	private String numero;
	private String bairro;
	private String cep;

	@Override
	public String toString() {
		return logradouro + ", " + numero + " - " + bairro + " CEP " + cep;
	}
}
