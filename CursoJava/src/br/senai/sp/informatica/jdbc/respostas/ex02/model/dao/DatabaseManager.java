package br.senai.sp.informatica.jdbc.respostas.ex02.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager {
	private static Connection con;
	
	private DatabaseManager() {
	}
	
	public static Connection getConnection() throws DaoException {
		try {
			if(con == null) {
				Class.forName("com.mysql.cj.jdbc.Driver");
	
				con = DriverManager.getConnection("jdbc:mysql://localhost:3307/javan1901"
						+ "?useTimezone=true&serverTimezone=UTC&useSSL=false", 
						"root", "root132");
			}
			return con;
		} catch(ClassNotFoundException ex) {
			ex.printStackTrace();
			throw new DaoException("Falha ao localizar o Driver JDBC");
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DaoException("Falha ao Conectar no Banco de Dados");
		}
	}
	
	public static void closeConnection() {
		try {
			if(con != null) {
				con.close();
				con = null;
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}
