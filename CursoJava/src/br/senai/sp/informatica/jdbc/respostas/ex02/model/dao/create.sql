-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema javan1901
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema javan1901
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `javan1901` DEFAULT CHARACTER SET latin1 ;
USE `javan1901` ;

-- -----------------------------------------------------
-- Table `javan1901`.`compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `javan1901`.`compra` (
  `idcompra` INT NOT NULL AUTO_INCREMENT,
  `data` DATE NOT NULL,
  PRIMARY KEY (`idcompra`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `javan1901`.`itemdecompra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `javan1901`.`itemdecompra` (
  `iditemdecompra` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NOT NULL,
  `quantidade` INT NOT NULL,
  `valor` DOUBLE NOT NULL,
  `idcompra` INT NOT NULL,
  PRIMARY KEY (`iditemdecompra`),
  INDEX `fk_itemdecompra_compra1_idx` (`idcompra` ASC),
  CONSTRAINT `fk_itemdecompra_compra1`
    FOREIGN KEY (`idcompra`)
    REFERENCES `javan1901`.`compra` (`idcompra`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
