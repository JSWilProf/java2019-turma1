package br.senai.sp.informatica.jdbc.respostas.ex02;

import java.awt.EventQueue;

import br.senai.sp.informatica.jdbc.respostas.ex02.view.TelaCompra;

public class Main {	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new TelaCompra();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
	}
}
