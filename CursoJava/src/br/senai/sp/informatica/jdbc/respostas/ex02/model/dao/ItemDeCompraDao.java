package br.senai.sp.informatica.jdbc.respostas.ex02.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.jdbc.respostas.ex02.model.ItemDeCompra;

public class ItemDeCompraDao {
	private static ItemDeCompraDao instance;
	private Connection con;
	private PreparedStatement insere;
	private PreparedStatement altera;
	private PreparedStatement remove;
	private PreparedStatement consulta;
	private PreparedStatement queryId;	

	private ItemDeCompraDao() throws DaoException {
		try {
			con = DatabaseManager.getConnection();
	
			consulta = con.prepareStatement("SELECT * FROM itemdecompra where idcompra=?");
			insere = con.prepareStatement("INSERT INTO itemdecompra (nome, quantidade, valor, idcompra) VALUES (?, ?, ?, ?)");
			altera = con.prepareStatement("UPDATE itemdecompra set nome=?, quantidade=?, valor=?, idcompra=? WHERE iditemdecompra=?");
			remove = con.prepareStatement("DELETE itemdecompra WHERE iditemdecompra=?");
			queryId = con.prepareStatement("select last_insert_id() as id"); 
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DaoException("A conexão com o Banco de Dados foi estabelecida");
		}
	}
	
	public static ItemDeCompraDao getInstance() throws DaoException {
		if(instance == null) {
			instance = new ItemDeCompraDao();
		}
		
		return instance;
	}
	
	public ItemDeCompra salvar(ItemDeCompra item, int idCompra) throws DaoException {
		String acao = "incluir";
		try {
			if(item.getId() == null) { // Incluir
				insere.setString(1, item.getNome());
				insere.setInt(2, item.getQuantidade());
				insere.setDouble(3, item.getValor());
				insere.setInt(4, idCompra);
				insere.execute();
				
				ResultSet result = queryId.executeQuery();
				if(result.next()) {
					item.setId(result.getInt("id")); 
				} else {
					throw new DaoException("Falha ao salvar o ItemDeCompra");
				}
			} else {                   // Atualizar
				acao = "atualizar";
				altera.setString(1, item.getNome());
				altera.setInt(2, item.getQuantidade());
				altera.setDouble(3, item.getValor());
				altera.setInt(4,  idCompra);
				altera.setInt(5, item.getId());
				altera.execute();
			}
			return item;
		} catch (SQLException ex) {
			throw new DaoException("Falha ao " + acao + " o ItemDeCompra");
		}
	}
	
	public void remover(int id) throws DaoException {
		try {
			remove.setInt(1, id);
			remove.execute();
		} catch (SQLException ex) {
			throw new DaoException("Falha ao excluir o ItemDeCompra");
		}		
	}

	public List<ItemDeCompra> listar(int idCompra) throws DaoException {
		try {
			List<ItemDeCompra> lista = new ArrayList<>();
			
			consulta.setInt(1, idCompra);
			ResultSet result = consulta.executeQuery();
			while (result.next()) {
				ItemDeCompra item = new ItemDeCompra();
				item.setId(result.getInt("iditemdecompra"));
				item.setNome(result.getString("nome"));
				item.setQuantidade(result.getInt("quantidade"));
				item.setValor(result.getDouble("valor"));
				
				lista.add(item);
			}
			return lista;
		} catch (SQLException ex) {
			throw new DaoException("Falha ao listar os ItemDeCompra");
		}		
	}
}
