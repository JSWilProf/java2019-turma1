package br.senai.sp.informatica.fundamentos;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class Ex0201 {
	public static void main(String[] args) {
		int horas = leInteiro("Informe o nº de Horas no mês");
		double salHora = leReal("Informe o Salário Hora");
		int dep = leInteiro("Informe o nº de dependentes");
		
		String msg = "Horas: " + horas +
					 String.format("\nSalário Hora: %,.2f", salHora) +
					 "\nNº de Dependentes: " + dep;
		
		double salBruto = horas * salHora + 50 * dep;
		
		msg += String.format("\nSalário Bruto: %,.2f", salBruto);
		
		double inss;
		if(salBruto <= 1000) {
			inss = salBruto * 0.085;
		} else {
			inss = salBruto * 0.09;
		}
		
		msg += String.format("\nINSS: %,.2f", inss);
		
		double ir;
		if(salBruto <= 500) {
			ir = 0;
		} else if(salBruto <= 1000) {
			ir = salBruto * 0.05;
		} else {
			ir = salBruto * 0.07;
		}

		msg += String.format("\nIR: %,.2f\nSalário Líquido: %,.2f",
				ir, salBruto - ir - inss);
		
		escreva(msg);
	}
}
