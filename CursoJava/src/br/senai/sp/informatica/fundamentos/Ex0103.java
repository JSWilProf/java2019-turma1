package br.senai.sp.informatica.fundamentos;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class Ex0103 {
	public static void main(String[] args) {
		double largura = leReal("Informe a Largura");
		double comprimento = leReal("Informe o comprimento");
		
		double area = comprimento * largura;
		
		escreva("A Área é: ", area);
	}
}
