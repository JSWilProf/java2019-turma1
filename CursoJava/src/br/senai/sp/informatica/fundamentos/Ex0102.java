package br.senai.sp.informatica.fundamentos;

import javax.swing.JOptionPane;

public class Ex0102 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe a 1ª quantidade");
		int qtd1 = Integer.parseInt(temp);
		
		temp = JOptionPane.showInputDialog("Informe o 1º valor");
		double val1 = Double.parseDouble(temp);

		temp = JOptionPane.showInputDialog("Informe a 2ª quantidade");
		int qtd2 = Integer.parseInt(temp);
		
		temp = JOptionPane.showInputDialog("Informe o 2º valor");
		double val2 = Double.parseDouble(temp);

		temp = JOptionPane.showInputDialog("Informe a 3ª quantidade");
		int qtd3 = Integer.parseInt(temp);
		
		temp = JOptionPane.showInputDialog("Informe o 3º valor");
		double val3 = Double.parseDouble(temp);
	
		double total = qtd1 * val1 + qtd2 * val2 + qtd3 * val3;
		
		JOptionPane.showMessageDialog(null, "O Total é: " + total);
	}
}
