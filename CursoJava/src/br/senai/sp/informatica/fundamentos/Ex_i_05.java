package br.senai.sp.informatica.fundamentos;

import javax.swing.JOptionPane;

import br.senai.sp.informatica.lib.CurrencyFormat;

public class Ex_i_05 {
	public static void main(String[] args) {
		CurrencyFormat formatador = new CurrencyFormat();
		
		String temp = JOptionPane.showInputDialog("Informe o Comprimento");
		double comp = formatador.parse(temp); 

		temp = JOptionPane.showInputDialog("Informe a Largura");
		double larg = formatador.parse(temp); 

		temp = JOptionPane.showInputDialog("Informe a Profundidade");
		double prof = formatador.parse(temp); 

		double volume = comp * larg * prof;
		final double preco = 45;
		double valorFinal = volume * preco;
		
		JOptionPane.showMessageDialog(null, "O valor final é de " + 
					formatador.format(valorFinal));
	}
}
