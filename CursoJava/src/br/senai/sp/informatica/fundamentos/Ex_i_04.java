package br.senai.sp.informatica.fundamentos;

import javax.swing.JOptionPane;

public class Ex_i_04 {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe o 1º nº");
		int num1 = Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 2º nº");
		int num2 = Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 3º nº");
		int num3 = Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 4º nº");
		int num4 = Integer.parseInt(temp);

		int soma = num1 + num2 + num3 + num4;
		double media = soma / 4;
		
		JOptionPane.showMessageDialog(null, "A soma é: " + soma + "\nA média é: " + media);
	}
}
