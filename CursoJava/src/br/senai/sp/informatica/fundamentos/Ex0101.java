package br.senai.sp.informatica.fundamentos;

import javax.swing.JOptionPane;

public class Ex0101 {
	public static void main(String[] args) {
		String nome1 = JOptionPane.showInputDialog("Informe o 1º Nome");
		String nome2 = JOptionPane.showInputDialog("Informe o 2º Nome");
		String nome3 = JOptionPane.showInputDialog("Informe o 3º Nome");
		
		JOptionPane.showMessageDialog(null, nome1 + " " + nome3 + "\n" + nome2);
	}
}
