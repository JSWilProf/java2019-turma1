package br.senai.sp.informatica.fundamentos;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class Ex_i_04d {
	public static void main(String[] args) {
		int soma = leInteiro("Informe o 1º nº") +
				   leInteiro("Informe o 2º nº") +
				   leInteiro("Informe o 3º nº") +
				   leInteiro("Informe o 4º nº");

		double media = soma / 4;
		
		escreva("A soma é: ", soma, "\nA média é: ", media);
	}
}
