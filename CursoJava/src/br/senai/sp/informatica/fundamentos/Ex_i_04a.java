package br.senai.sp.informatica.fundamentos;

import javax.swing.JOptionPane;

public class Ex_i_04a {
	public static void main(String[] args) {
		String temp = JOptionPane.showInputDialog("Informe o 1º nº");
		int soma = Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 2º nº");
		soma = soma + Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 3º nº");
		soma = soma +  Integer.parseInt(temp);

		temp = JOptionPane.showInputDialog("Informe o 4º nº");
		soma = soma +  Integer.parseInt(temp);

		double media = soma / 4;
		
		JOptionPane.showMessageDialog(null, "A soma é: " + soma + "\nA média é: " + media);
	}
}
