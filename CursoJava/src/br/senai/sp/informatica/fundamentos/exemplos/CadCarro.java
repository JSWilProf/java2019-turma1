package br.senai.sp.informatica.fundamentos.exemplos;

import static br.senai.sp.informatica.lib.SwUtil.escreva;
import static br.senai.sp.informatica.lib.SwUtil.leInteiro;
import static br.senai.sp.informatica.lib.SwUtil.leTexto;

public class CadCarro {
	public static void main(String[] args) {
		Carro[] garagem = new Carro[4];
		
		for (int vaga = 0; vaga < garagem.length; vaga++) {
			Carro novoCarro = new Carro();
			novoCarro.setMarca(leTexto("Informe a Marca"));
			novoCarro.setModelo(leTexto("Informe o Modelo"));
			novoCarro.setAno(leInteiro("Informe o Ano"));
			garagem[vaga] = novoCarro;
		}
		
		String msg = "Carros da minha Garagem\n\n";
		for (Carro carro : garagem) {
			msg += carro + "\n";
		}
		escreva(msg);
	}
}
