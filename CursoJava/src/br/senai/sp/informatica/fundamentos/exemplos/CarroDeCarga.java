package br.senai.sp.informatica.fundamentos.exemplos;

public class CarroDeCarga extends Carro {
	private double tara;

	public double getTara() {
		return tara;
	}

	public void setTara(double tara) {
		this.tara = tara;
	}

	@Override
	public String toString() {
		return super.toString() + " Tara: " + tara;
	}
}
