package br.senai.sp.informatica.fundamentos.exemplos;

import static br.senai.sp.informatica.lib.SwUtil.*;

import javax.swing.JOptionPane;

public class CadCarro2a {
	public static void main(String[] args) {
		Carro[] garagem = new Carro[4];
		
		for (int vaga = 0; vaga < garagem.length; vaga++) {
			int tipoCarro = JOptionPane.showOptionDialog(null, "Selecione o Tipo do Carro", "Garagem da Esquina", 
					JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
					new String[] {"Carro de Passeio", "Carro de Carga", "Sair"}, "Carro de Passeio");
			
			switch (tipoCarro) {
			case 0:
				// Carro de Passeio
				CarroDePasseio passeio = new CarroDePasseio();
				leDados(passeio);
				garagem[vaga] = passeio;
				break;
			case 1:
				// Carro de Passeio
				CarroDeCarga carga = new CarroDeCarga();
				leDados(carga);
				garagem[vaga] = carga;
				break;
			case 2:
				break;
			default:
				System.exit(0);
				break;
			}
			
		}
		
		String msg = "Carros da minha Garagem\n\n";
		for (Carro carro : garagem) {
			if(carro != null)
				msg += carro + "\n";
		}
		escreva(msg);
	}

	public static void leDados(Carro carro) {
		carro.setMarca(leTexto("Informe a Marca"));
		carro.setModelo(leTexto("Informe o Modelo"));
		carro.setAno(leInteiro("Informe o Ano"));
		
		if(carro instanceof CarroDePasseio) {
			CarroDePasseio passeio = (CarroDePasseio)carro;
			passeio.setLugares(leInteiro("Informe a quantidade de Lugares"));
			passeio.setPortamalas(leInteiro("Informe o tamanho do Porta-malas"));
		} else {
			CarroDeCarga carga = (CarroDeCarga)carro;
			carga.setTara(leReal("Informe a capacidade de Carga"));
		}
	}
}
