package br.senai.sp.informatica.fundamentos.exemplos;

public class CarroDePasseio extends Carro {
	private int lugares;
	private int portamalas;

	public int getLugares() {
		return lugares;
	}

	public void setLugares(int lugares) {
		this.lugares = lugares;
	}

	public int getPortamalas() {
		return portamalas;
	}

	public void setPortamalas(int portamalas) {
		this.portamalas = portamalas;
	}

	@Override
	public String toString() {
		return super.toString() + " Lugares: " + lugares + " Porta-malas: " + portamalas;
	}

}
