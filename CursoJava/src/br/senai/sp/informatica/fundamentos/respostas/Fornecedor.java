package br.senai.sp.informatica.fundamentos.respostas;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class Fornecedor extends Pessoa {
	private String cnpj;
	private String incricao;
	private String site;
}
