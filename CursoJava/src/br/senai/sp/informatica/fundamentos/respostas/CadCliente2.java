package br.senai.sp.informatica.fundamentos.respostas;

import static br.senai.sp.informatica.lib.SwUtil.escreva;
import static br.senai.sp.informatica.lib.SwUtil.leTexto;

import javax.swing.JOptionPane;
import java.util.List;
import java.util.ArrayList;

public class CadCliente2 {
	public static void main(String[] args) {
		List<Pessoa> cadastro = new ArrayList<>();
		
		for (;;) {
			int op = JOptionPane.showOptionDialog(null, "Selecione o Tipo de Pessoa", "Cadastro Geral", 
					JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
					new String[] {"Cliente", "Fornecedor", "Sair"}, "Cliente");

			if(op == 0) {
				Cliente cli = new Cliente();
				cli.setNome(leTexto("Informe a Nome"));
				cli.setEndereco(leTexto("Informe o End."));
				cli.setTelefone(leTexto("Informe o Fone"));
				cli.setCpf(leTexto("Informe o CPF"));
				cli.setEmail(leTexto("Informe o E-Mail"));
				cli.setRg(leTexto("Informe o RG"));
				cadastro.add(cli);
			} else if(op == 1) {
				Fornecedor forn = new Fornecedor();
				forn.setNome(leTexto("Informe a Nome"));
				forn.setEndereco(leTexto("Informe o End."));
				forn.setTelefone(leTexto("Informe o Fone"));
				forn.setCnpj(leTexto("Informe o CNPJ"));
				forn.setIncricao(leTexto("Informe o I.E."));
				forn.setSite(leTexto("Informe o Site"));
				cadastro.add(forn);			
			} else {
				break;
			}
		}
				
		if( ! cadastro.isEmpty()) {
			String msg = "Cadastro de Clientes\n\n";
			for (Pessoa carro : cadastro) {
				msg += carro + "\n";
			}
			escreva(msg);
		} else {
			escreva("O Cadastro está vazio!");
		}
	}
}
