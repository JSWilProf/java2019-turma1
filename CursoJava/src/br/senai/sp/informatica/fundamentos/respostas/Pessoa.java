package br.senai.sp.informatica.fundamentos.respostas;

import lombok.Data;

@Data
public class Pessoa {
	private String nome;
	private String endereco;
	private String telefone;


	@Override
	public String toString() {
		return "nome: " + nome + " endereco: " + endereco + " telefone: " + telefone;
	}
}
