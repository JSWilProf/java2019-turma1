package br.senai.sp.informatica.fundamentos.respostas;

import static br.senai.sp.informatica.lib.SwUtil.escreva;
import static br.senai.sp.informatica.lib.SwUtil.leTexto;

public class CadCliente {
	public static void main(String[] args) {
		Pessoa[] cadastro = new Pessoa[4];
		
		for (int vaga = 0; vaga < cadastro.length; vaga++) {
			Pessoa cli = new Pessoa();
			cli.setNome(leTexto("Informe a Nome"));
			cli.setEndereco(leTexto("Informe o End."));
			cli.setTelefone(leTexto("Informe o Fone"));
			cadastro[vaga] = cli;
		}
		
		String msg = "Cadastro de Clientes\n\n";
		for (Pessoa carro : cadastro) {
			msg += carro + "\n";
		}
		escreva(msg);
	}
}
