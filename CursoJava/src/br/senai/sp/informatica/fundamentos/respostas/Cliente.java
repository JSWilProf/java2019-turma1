package br.senai.sp.informatica.fundamentos.respostas;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class Cliente extends Pessoa {
	private String rg;
	private String cpf;
	private String email;
	
	@Override
	public String toString() {
		return super.toString() + " Rg: " + rg + " CPF: " + cpf + " E-Mail: " + email;
	}
}
