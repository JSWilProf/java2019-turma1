package br.senai.sp.informatica.fundamentos;

import javax.swing.JOptionPane;

import br.senai.sp.informatica.lib.CurrencyFormat;

public class Ex0102a {
	public static void main(String[] args) {
		CurrencyFormat formata = new CurrencyFormat();
		
		String temp = JOptionPane.showInputDialog("Informe a 1ª quantidade");
		int qtd1 = Integer.parseInt(temp);
		
		temp = JOptionPane.showInputDialog("Informe o 1º valor");
		double val1 = formata.parse(temp);

		temp = JOptionPane.showInputDialog("Informe a 2ª quantidade");
		int qtd2 = Integer.parseInt(temp);
		
		temp = JOptionPane.showInputDialog("Informe o 2º valor");
		double val2 = formata.parse(temp);

		temp = JOptionPane.showInputDialog("Informe a 3ª quantidade");
		int qtd3 = Integer.parseInt(temp);
		
		temp = JOptionPane.showInputDialog("Informe o 3º valor");
		double val3 = formata.parse(temp);
	
		double total = qtd1 * val1 + qtd2 * val2 + qtd3 * val3;
		
		JOptionPane.showMessageDialog(null, "O Total é: " + formata.format(total));
	}
}
