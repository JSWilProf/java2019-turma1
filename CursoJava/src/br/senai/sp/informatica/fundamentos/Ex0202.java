package br.senai.sp.informatica.fundamentos;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class Ex0202 {
	public static void main(String[] args) {
		int nota1 = leInteiro("Informe a 1ª nota");
		int nota2 = leInteiro("Informe a 2ª nota");
		double media = (nota1 + nota2) / 2;
		
		if(media <= 60) {
			escreva("Insuficiente");
		} else if(media <= 80) {
			escreva("Satisfatória");
		} else if(media <= 90) {
			escreva("Boa");
		} else {
			escreva("Excelente");
		}
	}
}
