package br.senai.sp.informatica.fundamentos;

import javax.swing.JOptionPane;

import br.senai.sp.informatica.lib.CurrencyFormat;

public class Ex0102b {
	static CurrencyFormat formata = new CurrencyFormat();

	public static void main(String[] args) {
		
		int qtd1 = leInteiro("Informe a 1ª quantidade");
		double val1 = leValor("Informe o 1º valor");

		int qtd2 = leInteiro("Informe a 2ª quantidade");
		double val2 = leValor("Informe o 2º valor");

		int qtd3 = leInteiro("Informe a 3ª quantidade");
		double val3 = leValor("Informe o 3º valor");
	
		double total = qtd1 * val1 + qtd2 * val2 + qtd3 * val3;
		
		JOptionPane.showMessageDialog(null, "O Total é: " + formata.format(total));
	}
	
	public static int leInteiro(String texto) {
		String temp = JOptionPane.showInputDialog(texto);
		return Integer.parseInt(temp);
	}
	
	public static double leValor(String texto) {
		String temp = JOptionPane.showInputDialog(texto);
		return formata.parse(temp);	
	}
}
