package br.senai.sp.informatica.fundamentos;

import static br.senai.sp.informatica.lib.SwUtil.*;

public class Ex_i_05a {
	public static void main(String[] args) {
		double comp = leReal("Informe o Comprimento");
		double larg = leReal("Informe a Largura"); 
		double prof = leReal("Informe a Profundidade");

		double volume = comp * larg * prof;
		final double preco = 45;
		double valorFinal = volume * preco;
		
		escreva("O valor final é de ", valorFinal);
	}
}
