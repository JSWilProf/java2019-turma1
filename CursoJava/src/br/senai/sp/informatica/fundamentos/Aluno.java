package br.senai.sp.informatica.fundamentos;

public class Aluno {
	private String nome;
	private int idade;
	private String email;
	private double[] notas;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double[] getNotas() {
		return notas;
	}

	public void setNotas(double[] notas) {
		this.notas = notas;
	}

	public double getMedia() {
		if (notas == null) {
			return 0;
		} else {
			double soma = 0;

			for (int i = 0; i < notas.length; i++) {
				soma += notas[i];
			}

			return soma / notas.length;
		}
	}
}
