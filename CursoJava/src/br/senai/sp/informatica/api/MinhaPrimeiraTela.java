package br.senai.sp.informatica.api;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MinhaPrimeiraTela {
	public static void main(String[] args) {
		// Cria um janela
		JFrame tela = new JFrame("Minha 1ª Tela");
		// Define uma largura e altura inicial para o janela
//		tela.setPreferredSize(new Dimension(300, 300));
//		tela.setMinimumSize(new Dimension(200, 200));
//		tela.setMaximumSize(new Dimension(500, 500));
		
		JPanel painel = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
		painel.add(new JLabel("Nome"));
		
		JTextField tfNome = new JTextField(15);
		painel.add(tfNome);
		
		tela.add(painel, BorderLayout.CENTER);
		
		JPanel painelDeBotoes = new JPanel();
		
		JButton btOk = new JButton("Ok");
		
		btOk.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent ev) {
				String nome = tfNome.getText();
				JOptionPane.showMessageDialog(tela, "Bem Vindo, " + nome);
				tfNome.setText("");
				tfNome.requestFocus();
			}
		});
		
		painelDeBotoes.add(btOk);
		
		JButton btSair = new JButton("Sair");
		btSair.addActionListener( ev -> System.exit(0) );
		
		
		painelDeBotoes.add(btSair);
		
		tela.add(painelDeBotoes, BorderLayout.SOUTH);
		
		tela.getRootPane().setDefaultButton(btOk);
		
		// Faz o ajuste do conteúdo da janela segundo o que foi configurado
		tela.pack();
		// Não permite o redimensionamento da janela
		tela.setResizable(false);
		// Centraliza a janela tomando como base a tela do computador
		tela.setLocationRelativeTo(null);
		// Declara que quando a janela for finalizada o programa java encerrará
		tela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Faz a janela ser apresentada
		tela.setVisible(true);
	}
}
