package br.senai.sp.informatica.api.empresa.modelo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Fornecedor {
	private String nome;
	private String endereco;

	@Override
	public String toString() {
		return "Nome: " + nome + " End.: " + endereco;
	}
}
