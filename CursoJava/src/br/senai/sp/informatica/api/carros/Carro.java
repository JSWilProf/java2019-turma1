package br.senai.sp.informatica.api.carros;

import lombok.Data;

@Data
public class Carro {
	private String marca;
	private String modelo;
	private int ano;

	@Override
	public String toString() {
		return "Marca:" + marca + " Modelo:" + modelo + " Ano" + ano;
	}
}
