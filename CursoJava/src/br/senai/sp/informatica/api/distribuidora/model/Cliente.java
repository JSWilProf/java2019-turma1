package br.senai.sp.informatica.api.distribuidora.model;

import java.util.List;

public class Cliente {
	private String nome;
	private Endereco endereco;
	private List<Compra> compras;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Compra> getCompras() {
		return compras;
	}

	public void setCompras(List<Compra> compras) {
		this.compras = compras;
	}

	@Override
	public String toString() {
		return "nome: " + nome + " endereco: " + endereco + " compras: " + compras;
	}

}
