package br.senai.sp.informatica.api.distribuidora.model;

import java.util.List;

public class Fornecedor {
	private String nome;
	private Endereco endereco;
	private List<Produto> pridutos;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Produto> getPridutos() {
		return pridutos;
	}

	public void setPridutos(List<Produto> pridutos) {
		this.pridutos = pridutos;
	}

	@Override
	public String toString() {
		return "nome: " + nome + " endereco: " + endereco + " pridutos: " + pridutos;
	}

}
