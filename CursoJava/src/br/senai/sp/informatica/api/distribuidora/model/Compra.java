package br.senai.sp.informatica.api.distribuidora.model;

import java.util.Date;
import java.util.List;

public class Compra {
	private Date data;
	private Cliente cliente;
	private List<ItemDeCompra> itens;

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemDeCompra> getItens() {
		return itens;
	}

	public void setItens(List<ItemDeCompra> itens) {
		this.itens = itens;
	}

	@Override
	public String toString() {
		return "data: " + data + " cliente: " + cliente + " itens: " + itens;
	}

}
