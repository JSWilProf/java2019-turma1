package br.senai.sp.informatica.api;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class MinhaTerceiraTela extends JFrame implements ActionListener {
	private JTextField tfNome = new JTextField(15);
	private JButton btOk = new JButton("Ok");
	private JButton btSair = new JButton("Sair");
	
	public MinhaTerceiraTela() {
		setTitle("Minha 2ª Tela");

		JPanel painel = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
		painel.add(new JLabel("Nome"));
		painel.add(tfNome);
		add(painel, BorderLayout.CENTER);

		JPanel painelDeBotoes = new JPanel();
		btOk.addActionListener(this);
		btSair.addActionListener(this);
		painelDeBotoes.add(btOk);
		painelDeBotoes.add(btSair);
		add(painelDeBotoes, BorderLayout.SOUTH);
		
		getRootPane().setDefaultButton(btOk);
		
		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		Object botao = ev.getSource();
		
		if(botao.equals(btOk)) {
			String nome = tfNome.getText();
			JOptionPane.showMessageDialog(this, "Bem Vindo, " + nome);
			tfNome.setText("");
			tfNome.requestFocus();
		} else {
			System.exit(0);
		}
	}
	
	public static void main(String[] args) {
		new MinhaTerceiraTela();
	}
}
