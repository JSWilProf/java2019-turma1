package br.senai.sp.informatica.api.escola.model;

import lombok.Data;

@Data
public class Disciplina {
	private String nome;
	private Curso curso;

	@Override
	public String toString() {
		return "Nome: " + nome;
	}
}
