package br.senai.sp.informatica.api.escola.view;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.senai.sp.informatica.api.escola.model.Disciplina;

@SuppressWarnings("serial")
public class DisciplinaModel extends AbstractTableModel {
	private String[] titulos = { "Nome da Disciplina" } ;
	private List<Disciplina> lista;
 	
	public DisciplinaModel(List<Disciplina> disciplinas) {
		lista = disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		lista = disciplinas;
		fireTableDataChanged();
	}
	
	@Override
	public int getRowCount() {
		return lista.size();
	}

	@Override
	public int getColumnCount() {
		return titulos.length;
	}

	@Override
	public String getColumnName(int coluna) {
		return titulos[coluna];
	}

	@Override
	public Object getValueAt(int linha, int coluna) {
		Object valor = null;
		Disciplina disciplina = lista.get(linha);
		
		switch (coluna) {
		case 0:
			valor = disciplina.getNome();
			break;
		}
		
		return valor;
	}
}
