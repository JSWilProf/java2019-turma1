package br.senai.sp.informatica.api.escola.model;

import java.util.List;

import lombok.Data;

@Data
public class Curso {
	private String nome;
	private List<Disciplina> disciplinas;

	@Override
	public String toString() {
		String msg =  "Curso: " + nome + "\nDisciplinas:\n";
		for (Disciplina disciplina : disciplinas) {
			msg += disciplina + "\n";
		}
		return msg;
	}
}
