package br.senai.sp.informatica.api.escola.view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import br.senai.sp.informatica.api.escola.model.Curso;
import br.senai.sp.informatica.api.escola.model.Disciplina;
import br.senai.sp.informatica.lib.SwUtil;
import javax.swing.JScrollPane;
import javax.swing.JTable;

@SuppressWarnings("serial")
public class CadCursoTabela extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblNome;
	private JTextField tfNome;
	private JButton btnDisciplinas;
	private JButton btnSalvar;
	private JButton btnListar;
	private JButton btnSair;
	private JScrollPane scrollPane;
	private JTable table;

	private List<Curso> cadastro = new ArrayList<>();
	private List<Disciplina> disciplinas = new ArrayList<>();
	private DisciplinaModel model = new DisciplinaModel(disciplinas); 
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadCursoTabela frame = new CadCursoTabela();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public CadCursoTabela() {
		setTitle("Cadastro de Cursos");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 277);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		lblNome = new JLabel("Nome");
		
		tfNome = new JTextField();
		tfNome.setColumns(10);
		
		btnDisciplinas = new JButton("Disciplinas");
		btnDisciplinas.addActionListener(this);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(this);
		
		btnListar = new JButton("Listar");
		btnListar.addActionListener(this);
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(this);
		
		scrollPane = new JScrollPane();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 422, Short.MAX_VALUE)
							.addGap(12))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblNome)
									.addGap(18)
									.addComponent(tfNome, GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE))
								.addComponent(btnDisciplinas)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(btnSalvar)
									.addGap(97)
									.addComponent(btnListar)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnSair))))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNome)
						.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(btnDisciplinas)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSalvar)
						.addComponent(btnSair)
						.addComponent(btnListar))
					.addContainerGap())
		);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);
		table.setModel(model);
		scrollPane.setViewportView(table);
		contentPane.setLayout(gl_contentPane);
	}

	public void actionPerformed(ActionEvent ev) {
		Object botao = ev.getSource();
		
		if(botao.equals(btnSalvar)) {
			Curso curso = new Curso();
			curso.setNome(tfNome.getText());
			curso.setDisciplinas(disciplinas);
			
			for (Disciplina disciplina : disciplinas) {
				disciplina.setCurso(curso);
			}
			
			cadastro.add(curso);
			
			disciplinas = new ArrayList<>();
			model.setDisciplinas(disciplinas);
			SwUtil.limpa(this);
			tfNome.requestFocus();
		} else if(botao.equals(btnListar)) {
			String msg = "Cadastro de Cursos\n\n";
			for (Curso curso : cadastro) {
				msg += curso + "\n";
			}
			JOptionPane.showMessageDialog(this, msg);
		} else if(botao.equals(btnDisciplinas)) {
			Disciplina disciplina = new Disciplina();
			
			CadDisciplina dialog = new CadDisciplina(disciplina);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setLocationRelativeTo(this);
			dialog.setVisible(true);
			
			if(disciplina.getNome() != null) {
				disciplinas.add(disciplina);
				model.fireTableDataChanged();
			}
		} else {
			System.exit(0);
		}
	}
}

