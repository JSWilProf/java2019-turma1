package br.senai.sp.informatica.api;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class MinhaQuartaTela extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblNome;
	private JTextField tfNome;
	private JButton btnOk;
	private JButton btnSair;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MinhaQuartaTela frame = new MinhaQuartaTela();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MinhaQuartaTela() {
		setTitle("Minha 4ª Tela");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 330, 109);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		lblNome = new JLabel("Nome");
		lblNome.setBounds(14, 7, 37, 16);
		
		tfNome = new JTextField();
		tfNome.setBounds(65, 2, 249, 26);
		tfNome.setColumns(10);
		
		btnOk = new JButton("Ok");
		btnOk.setBounds(60, 52, 75, 29);
		btnOk.addActionListener(this);
		
		btnSair = new JButton("Sair");
		btnSair.setBounds(195, 52, 75, 29);
		btnSair.addActionListener(this);
		contentPane.setLayout(null);
		contentPane.add(lblNome);
		contentPane.add(tfNome);
		contentPane.add(btnOk);
		contentPane.add(btnSair);
		
		getRootPane().setDefaultButton(btnOk);
		setLocationRelativeTo(null);
	}
	
	public void actionPerformed(ActionEvent ev) {
		Object botao = ev.getSource();
		
		if(botao.equals(btnOk)) {
			String nome = tfNome.getText();
			JOptionPane.showMessageDialog(this, "Bem Vindo, " + nome);
			tfNome.setText("");
			tfNome.requestFocus();
		} else {
			System.exit(0);
		}

	}
}
